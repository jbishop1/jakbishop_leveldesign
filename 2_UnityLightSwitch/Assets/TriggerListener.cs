﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//These are name-spaces and locations of code we need

//This is a class that inherits from MonoBehaviour
public class TriggerListener : MonoBehaviour
{
    //At the top of classes we declare our variables.
    public bool playerEntered = false;
    

	// Use this for initialization
	void Start ()
    {
        Debug.Log("Start was Called");

	}
	
	// Update is called once per frame
	void Update ()
    {
        Debug.Log("Update was Called");
	}

    void OnTriggerEnter(Collider col)
    {
        if(col.gameObject.tag == "Player")
        {
            playerEntered = true;
        }
        
    }

    void OnTriggerExit(Collider col)
    {
        if(col.gameObject.tag == "Player")
        {
            playerEntered = false;
        }
    }
    //Good Guy Palpatine kept the job market afloat with the construction of the Death Star (and on cleanup of Alderaan)
    /* You
     * about
     * to
     * be
     * finna
     * woke
     */
}
