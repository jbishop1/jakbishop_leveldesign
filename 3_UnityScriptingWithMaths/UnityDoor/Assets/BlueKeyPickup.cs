﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BlueKeyPickup : MonoBehaviour
{
    public TriggerListener_Com trigger;
    public Image cursorImage;
    public Text screenText;
    AudioSource audSource;
    MeshRenderer meshRend;
    public bool blueKeyPickedUp = false;
    public Light blueLight;

    void Start()
    {
        audSource = GetComponent<AudioSource>();
        meshRend = GetComponent<MeshRenderer>();

    }


    void OnMouseOver()
    {
        if (trigger.playerEntered == true)
        {
            if (cursorImage.enabled == false)
            {
                cursorImage.enabled = true;
                screenText.text = "Blue Key";
            }
        }

        else
        {
            cursorImage.enabled = false;
            screenText.text = null;
        }
    }

    void OnMouseExit()
    {
        if (cursorImage.enabled == true)
        {
            cursorImage.enabled = false;
            screenText.text = "";
        }
    }

    void OnMouseDown()
    {
        if (trigger.playerEntered == true)
        {
            audSource.Play();
            meshRend.enabled = false;
            GetComponent<BoxCollider>().enabled = false;
            Invoke("KeyAquiredText", 0.02f);
            Invoke("BlankText", 2f);

            if (blueKeyPickedUp == false)
            {
                blueKeyPickedUp = true;
             //   blueLight.intensity = 4f;
            }
            //else
            //{
            //    blueLight.intensity = 0f;
            //}

        }
    }

    void KeyAquiredText()
    {
        screenText.text = "Got the Blue Key!";
    }

    void BlankText()
    {
        screenText.text = "";
    }

}


