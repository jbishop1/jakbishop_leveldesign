﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisarmTheBomb : MonoBehaviour
{
    public TriggerListener_Com trigger;
    public YellowKeyScript useYellowKey;
    public GreenKeyScript useGreenKey;
    public BlueKeyPickup useBlueKey;
    public RedKeyScript useRedKey;
    public Text screenText;
    public Image cursorImage;
    MeshRenderer meshRend;

    // Use this for initialization
    void Start ()
    {
        meshRend = GetComponent<MeshRenderer>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (useGreenKey.greenKeyPickedUp == true && useBlueKey.blueKeyPickedUp == true && useRedKey.redKeyPickedUp == true && useYellowKey.yellowKeyPickedUp == true)
        {
            screenText.text = "You Disarmed the Bomb!";
            Invoke("BlankText", 3f);
            
        }

 
    }

    void BlankText()
    {
        screenText.text = "";
    }

    void OnMouseOver()
    {
        if (trigger.playerEntered == true)
        {
            if (cursorImage.enabled == false)
            {
                cursorImage.enabled = true;
                screenText.text = "Find all four keys !";
            }
            else if (trigger.playerEntered == true && useGreenKey.greenKeyPickedUp == true && useBlueKey.blueKeyPickedUp == true && useRedKey.redKeyPickedUp == true && useYellowKey.yellowKeyPickedUp == true)
            {
                cursorImage.enabled = true;
                screenText.text = "You Disarmed the Bomb!";
            }
        }

        else
        {
            cursorImage.enabled = false;
            screenText.text = null;
        }
    }

    void OnMouseExit()
    {
        if (cursorImage.enabled == true)
        {
            cursorImage.enabled = false;
            screenText.text = "";
        }
    }
}
