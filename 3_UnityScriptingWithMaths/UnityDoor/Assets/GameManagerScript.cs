﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManagerScript : MonoBehaviour
{
    public Text narrativeText;
	

	void Start ()
    {
        Invoke("IntroText01", 03);
	}
	

    void Introtext01()
    {
        narrativeText.text = "Ringo Midnight: Espionage Extrodinaire!";
        Invoke("IntroText02", 03);
    }


    void Introtext02()
    {
        narrativeText.text = "This week's Episode:";
        Invoke("IntroText03", 03);
    }


    void Introtext03()
    {
        narrativeText.text = "Manic Entrapment of Professor Mania!";
        Invoke("BlankText", 03);
    }


    void BlankText()
    {
        narrativeText = null;
    }

}
