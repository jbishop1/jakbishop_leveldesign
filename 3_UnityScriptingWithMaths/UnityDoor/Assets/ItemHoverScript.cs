﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemHoverScript : MonoBehaviour
{
    public float moveAmount = 1f;
    public float moveSpeed = 1f;
    public float rotSpeed = 1f;
    Vector3 startPos;

    void Start ()
    {
        startPos = transform.position;
	}
	
	
	void Update ()
    {
        transform.position = new Vector3(startPos.x, startPos.y + (Mathf.Sin(Time.time * moveSpeed) * moveAmount), startPos.z);
        transform.rotation = Quaternion.Euler(new Vector3(0f, (Time.time * rotSpeed), 0f));
	}
}
