﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KeyPickUpScript : MonoBehaviour
{
    public TriggerListener_Com trigger;
    public Image cursorImage;
    public Text screenText;
    AudioSource audSource;
    MeshRenderer meshRend;
    public bool keyPickedUp = false;

	void Start ()
    {
        audSource = GetComponent<AudioSource>();
        meshRend = GetComponent<MeshRenderer>();
	}


    void OnMouseOver()
    {
        if (trigger.playerEntered == true)
        {
            if(cursorImage.enabled == false)
            {
                cursorImage.enabled = true;
                screenText.text = "Key";
            }
        }

        else
        {
            cursorImage.enabled = false;
            screenText.text = null;
        }
    }

    void OnMouseExit()
    {
        if (cursorImage.enabled == true)
        {
            cursorImage.enabled = false;
            screenText.text = "";
        }
    }

    void OnMouseDown()
    {
        if (trigger.playerEntered == true)
        {
            audSource.Play();
            keyPickedUp = true;
            meshRend.enabled = false;
            GetComponent<BoxCollider>().enabled = false;
            Invoke("KeyAquiredText", 0.02f);
            Invoke("BlankText", 2f);

        }
    }

    void KeyAquiredText()
    {
        screenText.text = "Got the Key!";
    }

    void BlankText()
    {
        screenText.text = "";
    }

}
