﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class RedKeyScript : MonoBehaviour
{
    public TriggerListener_Com trigger;
    public Image cursorImage;
    public Text screenText;
    AudioSource audSource;
    MeshRenderer meshRend;
    public bool redKeyPickedUp = false;
    public Light redLight;

    void Start()
    {
        audSource = GetComponent<AudioSource>();
        meshRend = GetComponent<MeshRenderer>();
        redLight = GetComponent<Light>();


    }


    void OnMouseOver()
    {
        if (trigger.playerEntered == true)
        {
            if (cursorImage.enabled == false)
            {
                cursorImage.enabled = true;
                screenText.text = "Red Key";
            }
        }

        else
        {
            cursorImage.enabled = false;
            screenText.text = null;
        }
    }

    void OnMouseExit()
    {
        if (cursorImage.enabled == true)
        {
            cursorImage.enabled = false;
            screenText.text = "";
        }
    }

    void OnMouseDown()
    {
        if (trigger.playerEntered == true)
        {
            audSource.Play();
            meshRend.enabled = false;
            GetComponent<BoxCollider>().enabled = false;
            Invoke("KeyAquiredText", 0.02f);
            Invoke("BlankText", 2f);

            if (redKeyPickedUp == false)
            {
                redKeyPickedUp = true;
            //    redLight.intensity = 4f;
            }
            //else
            //{
            //    redLight.intensity = 0f;
            //}

        }
    }

    void KeyAquiredText()
    {
        screenText.text = "Got the Red Key!";
    }

    void BlankText()
    {
        screenText.text = "";
    }

}


