﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpinScript : MonoBehaviour
{
    public float rotSpeed = 1f;
    Vector3 startPos;


    void Start ()
    {
        startPos = transform.position;
    }
	

	void Update ()
    {
        transform.rotation = Quaternion.Euler(new Vector3(0f, (Time.time * rotSpeed), 0f));
    }
}
