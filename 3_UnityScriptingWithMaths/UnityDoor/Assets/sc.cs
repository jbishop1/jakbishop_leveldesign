﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class sc : MonoBehaviour
{
    bool inKillBox;
    GameObject myPlayer;
    Vector3 playerStart;
    Collider KillBox;



    // Use this for initialization
    void Start()
    {
        inKillBox = false;
        myPlayer = GameObject.FindGameObjectWithTag("Player");
        playerStart = myPlayer.transform.localPosition;

    }

    // Update is called once per frame
    void Update()
    {
        if (inKillBox == true)
        {
            myPlayer.transform.localPosition = playerStart;
            inKillBox = false;
        }
    }

    void OnTriggerEnter(Collider KillBox)
    {
        if (KillBox.gameObject.tag == "Player")
        {
            inKillBox = true;
        }
    }
}
